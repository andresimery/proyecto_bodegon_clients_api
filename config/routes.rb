Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      get 'clients', to: 'clients#index'
      get 'clients/:id', to: 'clients#show'
      post 'clients', to: 'clients#create'
      put 'clients/:id', to: 'clients#update'
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
