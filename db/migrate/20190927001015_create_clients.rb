class CreateClients < ActiveRecord::Migration[6.0]
  def change
    create_table :clients do |t|
      t.string :full_name, null: false
      t.string :address, null: false
      t.string :dni, null: false

      t.timestamps
    end
  end
end
