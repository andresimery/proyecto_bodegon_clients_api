class ClientSerializer < ActiveModel::Serializer
  attributes :id, :full_name, :address, :dni
end
