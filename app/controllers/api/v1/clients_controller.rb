module Api 
  module V1
    class ClientsController < ApplicationController
      def index
        render json: Client.all
      end

      def show
        client = Client.find_by(id: params[:id])
        if client.present?
          render json: client, status: :ok
        else
          render json: { error: 'Not Found' }, status: :not_found
        end
      end

      def create
        client = Client.new(client_params)
        if client.save
          render json: client, status: :created
        else
          render json: { error: 'Bad request' }, status: :bad_request
        end
      end

      def update
        client = Client.find_by(id: params[:id])
        if client.present?
          client.update(client_params)
          render json: client
        else
          render json: { error: 'Not Found' }, status: :not_found
        end
      end

      private

      def client_params
        params.permit(:full_name, :address, :dni)
      end
    end
  end
end